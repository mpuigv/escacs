package a;

import java.util.ArrayList;
import java.util.Scanner;

public class Escacs {
	
	public static Scanner sc = new Scanner(System.in);
	public static final int TAMANY_TAULER = 8;
	
	public static void main(String[] args) {
		
		boolean funcionant = true;
		do {
			
			System.out.println("Selecciona la funció que vols fer servir: ");
			System.out.println("1.- Calcular els possibles moviments.");
			System.out.println("2.- Comprovar si dues peces es defensen mútuament.");
			System.out.println("3.- Comprovar si una casella és controlada per dues peces a la vegada.");
			System.out.println("0.- Sortir");

			int opcio = sc.nextInt();
			sc.nextLine();	
			
			switch (opcio) {
			case 1:
				
				ArrayList<Integer> possiblesMoviments = new ArrayList<Integer>();
			
				char peca = demanaPeca();
				int posicio = demanaCasella();
				
				//metode moviments
				
			
				possiblesMoviments = moviments(peca, posicio);
				
				System.out.print("Aquests són els moviments possibles: ");
				System.out.println(possiblesMoviments);
					
				break;
			case 2:
				//metode defensades
				
				break;
			case 3:
				//metode comunes
				 
			case 0:
				System.out.println("Has sortit del programa...");
				funcionant = false;
				break;
			}
			
		} while (funcionant);
		
	}

	/**
	 * Mètode que donada una peça i una casella retorna tots els moviments possibles que té aquesta peça.
	 * @param peca 
	 * 		  Peça dels escacs.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> moviments(char peca, int posicio) {
		
		ArrayList<Integer> possiblesMoviments = new ArrayList<Integer>();
		
		switch (peca) {
		case 'C':
			possiblesMoviments = cavall(posicio);
			break;
		case 'A':
			possiblesMoviments = alfil(posicio);
			break;
		case 'T':
			possiblesMoviments = torre(posicio);
			break;
		case 'D':
			possiblesMoviments = dama(posicio);
			break;
		case 'P':
			possiblesMoviments = peo(posicio);
			break;
		case 'R':
			possiblesMoviments = rei(posicio);
			break;
		}
		
		return possiblesMoviments;
	}
	
	/**
	 * Mètode que demana a l'usuari escollir una peça dels escacs.
	 * @return casella
	 *        Char que representa el tipus de peça escollida per l'usuari.
	 */
	public static char demanaPeca() {
		
		System.out.println("Selecciona una peça de les diponibles: ");
		System.out.println("'C' - Cavall");
		System.out.println("'A' - Alfil");
		System.out.println("'T' - Torre");
		System.out.println("'D' - Dama");
		System.out.println("'P' - Peó");
		System.out.println("'R' - Rei");
		System.out.println("'S' - Sortir");
		System.out.print("Escull una peça: ");
		
		char peca = sc.nextLine().charAt(0);
		
		return peca;
	}
	
	/**
	 * Mètode que demana a l'usuari introduir una casella en format FC(fila-columna).
	 * @return casella
	 *        Integer que representa la casella introduida per l'usuari.
	 */
	public static int demanaCasella() {
		
		int casella = 0;
		
		boolean demanantCasella = true;
		while (demanantCasella) {
			
			System.out.println("Introdueix la posició de la peça(format FC): ");
			casella = sc.nextInt();
			if (validaCasella(casella) == true) {
				demanantCasella = false;
			} else {
				System.out.println("Aquesta posició no és valida.");
				
			}
		}
		
		return casella;
	}
	
	/**
	 * Mètode que donada una casella retorna true si es valida i false si no ho es.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return esValida
	 *        Boolean indicant si la validació es correcta.
	 */
	public static boolean validaCasella(int posicio) {
		
		int fila = posicio / 10;
		int columna = posicio % 10;
		
		if ((fila <= 8 && fila >= 1) && (columna <= 8 && columna >= 1)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té el peó.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> peo(int posicio) {
		
		ArrayList<Integer> possiblesMoviments = new ArrayList<Integer>();
		
		int fila = posicio / 10;
		int columna = posicio % 10;
		
		
		if (fila == 2) {
			int novaPosicio = ((fila + 1) * 10) + columna;
			if (validaCasella(novaPosicio) == true) {
				possiblesMoviments.add(novaPosicio);
			}
			novaPosicio = ((fila + 2) * 10) + columna;
			if (validaCasella(novaPosicio) == true) {
				possiblesMoviments.add(novaPosicio);
			}
			
		} else {
			int novaPosicio = ((fila + 1) * 10) + columna;
			if (validaCasella(novaPosicio) == true) {
				possiblesMoviments.add(novaPosicio);
			}
		}
		
		return possiblesMoviments;
	}
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té el rei.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> rei(int posicio) {
		
		ArrayList<Integer> posiblesMoviments = new ArrayList<Integer>();
		
		int fila = posicio / 10;
		int columna = posicio % 10;
		
		//superior
		int novaPosicio = ((fila + 1) * 10) + columna;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//superior dreta
		novaPosicio = ((fila + 1) * 10) + columna+1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//dreta
		novaPosicio = ((fila) * 10) + columna+1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//inferior dreta
		novaPosicio = ((fila - 1) * 10) + columna+1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//inferior
		novaPosicio = ((fila - 1) * 10) + columna;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//inferior esquerra
		novaPosicio = ((fila - 1) * 10) + columna-1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//esquerra
		novaPosicio = ((fila) * 10) + columna-1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		//superior esquerra
		novaPosicio = ((fila + 1) * 10) + columna-1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		
		return posiblesMoviments;
	}
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té la torre.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> torre(int posicio) {
		
		ArrayList<Integer> posiblesMoviments = new ArrayList<Integer>();
		
		int fila = posicio / 10;
		int columna = posicio % 10;
		
		//superior
		for (int i = fila+1; i <= TAMANY_TAULER; i++) {
			int novaPosicio = ((i * 10) + columna);
			posiblesMoviments.add(novaPosicio);
		}
		//dreta
		for (int i = columna+1; i <= TAMANY_TAULER; i++) {
			int novaPosicio = ((fila * 10) + i);
			posiblesMoviments.add(novaPosicio);
		}
		//inferior
		for (int i = fila-1; i >= 1; i--) {
			int novaPosicio = ((i * 10) + columna);
			posiblesMoviments.add(novaPosicio);
		}
		//esquerra
		for (int i = columna-1; i >= 1; i--) {
			int novaPosicio = ((fila * 10) + i);
			posiblesMoviments.add(novaPosicio);
		}
		
		
		return posiblesMoviments;
	}
	
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té el cavall.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> cavall(int posicio) {
		
		ArrayList<Integer> posiblesMoviments = new ArrayList<Integer>();
		
		int fila = posicio / 10;
		int columna = posicio % 10;
		
		//superior
		int novaPosicio = ((fila + 2) * 10) + columna-1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		novaPosicio = ((fila + 2) * 10) + columna+1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		
		//dreta
		novaPosicio = ((fila - 1) * 10) + columna+2;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		novaPosicio = ((fila + 1) * 10) + columna+2;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		
		//inferior
		novaPosicio = ((fila - 2) * 10) + columna-1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		novaPosicio = ((fila - 2) * 10) + columna+1;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		
		//esquerra
		novaPosicio = ((fila - 1) * 10) + columna-2;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		novaPosicio = ((fila + 1) * 10) + columna-2;
		if (validaCasella(novaPosicio) == true) {
			posiblesMoviments.add(novaPosicio);
		}
		
		return posiblesMoviments;
	}
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té l'alfil.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> alfil(int posicio) {
		
		ArrayList<Integer> posiblesMoviments = new ArrayList<Integer>();

		int fila = posicio / 10;
		int columna = posicio % 10;
		 
		//superior dreta
		boolean buscant = true;
		int i = 1;			
		while (buscant) {
			int novaFila = fila + i;
			int novaColumna = columna + i;
			
			int novaPosicio = ((novaFila) * 10) + novaColumna;
			if (validaCasella(novaPosicio)) {
				posiblesMoviments.add(novaPosicio);
				i++;
			} else {
				buscant = false;
			}
		}
		
		//inferior dreta	
		buscant = true;
		i = 1;
		while (buscant) {
			int novaFila = fila - i;
			int novaColumna = columna + i;
			
			int novaPosicio = ((novaFila) * 10) + novaColumna;
			if (validaCasella(novaPosicio)) {
				posiblesMoviments.add(novaPosicio);
				i++;
			} else {
				buscant = false;
			}
		}
		
		//inferior esquerra	
		buscant = true;
		i = 1;
		while (buscant) {
			int novaFila = fila - i;
			int novaColumna = columna - i;
			
			int novaPosicio = ((novaFila) * 10) + novaColumna;
			if (validaCasella(novaPosicio)) {
				posiblesMoviments.add(novaPosicio);
				i++;
			} else {
				buscant = false;
			}
		}
		
		//superior esquerra	
		buscant = true;
		i = 1;
		while (buscant) {
			int novaFila = fila + i;
			int novaColumna = columna - i;
			
			int novaPosicio = ((novaFila) * 10) + novaColumna;
			if (validaCasella(novaPosicio)) {
				posiblesMoviments.add(novaPosicio);
				i++;
			} else {
				buscant = false;
			}
		}		
		
		return posiblesMoviments;
	}
	
	/**
	 * Mètode que donada una casella retorna tots els moviments que té la dama.
	 * @param posicio 
	 * 		  Posició de la casella de partida.
	 * @return posiblesMoviments
	 *        ArrayList amb el moviments possibles.
	 */
	public static ArrayList<Integer> dama(int posicio) {
		
		ArrayList<Integer> posiblesMoviments = new ArrayList<Integer>();

		ArrayList<Integer> movimentsTorre = torre(posicio);
		ArrayList<Integer> movimentsAlfil = alfil(posicio);
		
		posiblesMoviments.addAll(movimentsTorre);
		posiblesMoviments.addAll(movimentsAlfil);
		
		return posiblesMoviments;
	}
	
	
	
	
}
