package a;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class EscacsTest {

	@Test
	void test() {
		// validacio de casella
		assertEquals(true, Escacs.validaCasella(44));
		assertEquals(false, Escacs.validaCasella(30));
		assertEquals(false, Escacs.validaCasella(-42));
		
		// cavall
		assertArrayEquals(Arrays.asList(63, 65, 36, 56, 23, 25, 32, 52).toArray(), Escacs.cavall(44).toArray());
		assertArrayEquals(Arrays.asList(32, 23).toArray(), Escacs.cavall(11).toArray());
		assertArrayEquals(Arrays.asList(73, 62).toArray(), Escacs.cavall(81).toArray());
		assertArrayEquals(Arrays.asList(67, 76).toArray(), Escacs.cavall(88).toArray());
		assertArrayEquals(Arrays.asList(37, 26).toArray(), Escacs.cavall(18).toArray());
		assertArrayEquals(Arrays.asList(33, 35, 26, 22).toArray(), Escacs.cavall(14).toArray());

		// alfil
		assertArrayEquals(Arrays.asList(55, 66, 77, 88, 35, 26, 17, 33, 22, 11, 53, 62, 71).toArray(), Escacs.alfil(44).toArray());
		assertArrayEquals(Arrays.asList(22, 33, 44, 55, 66, 77, 88).toArray(), Escacs.alfil(11).toArray());
		assertArrayEquals(Arrays.asList(72, 63, 54, 45, 36, 27, 18).toArray(), Escacs.alfil(81).toArray());
		assertArrayEquals(Arrays.asList(77, 66, 55, 44, 33, 22, 11).toArray(), Escacs.alfil(88).toArray());
		assertArrayEquals(Arrays.asList(27, 36, 45, 54, 63, 72, 81).toArray(), Escacs.alfil(18).toArray());
		assertArrayEquals(Arrays.asList(25, 36, 47, 58, 23, 32, 41).toArray(), Escacs.alfil(14).toArray());
		
		// torre
		assertArrayEquals(Arrays.asList(54, 64, 74, 84, 45, 46, 47, 48, 34, 24, 14, 43, 42, 41).toArray(), Escacs.torre(44).toArray());
		assertArrayEquals(Arrays.asList(21, 31, 41, 51, 61, 71, 81, 12, 13, 14, 15, 16, 17, 18).toArray(), Escacs.torre(11).toArray());
		assertArrayEquals(Arrays.asList(82, 83, 84, 85, 86, 87, 88, 71, 61, 51, 41, 31, 21, 11).toArray(), Escacs.torre(81).toArray());
		assertArrayEquals(Arrays.asList(78, 68, 58, 48, 38, 28, 18, 87, 86, 85, 84, 83, 82, 81).toArray(), Escacs.torre(88).toArray());
		assertArrayEquals(Arrays.asList(28, 38, 48, 58, 68, 78, 88, 17, 16, 15, 14, 13, 12, 11).toArray(), Escacs.torre(18).toArray());
		assertArrayEquals(Arrays.asList(24, 34, 44, 54, 64, 74, 84, 15, 16, 17, 18, 13, 12, 11).toArray(), Escacs.torre(14).toArray());
		
		// dama
		assertArrayEquals(Arrays.asList(54, 64, 74, 84, 45, 46, 47, 48, 34, 24, 14, 43, 42, 41, 55, 66, 77, 88, 35, 26, 17, 33, 22, 11, 53, 62, 71).toArray(), Escacs.dama(44).toArray());
		assertArrayEquals(Arrays.asList(21, 31, 41, 51, 61, 71, 81, 12, 13, 14, 15, 16, 17, 18, 22, 33, 44, 55, 66, 77, 88).toArray(), Escacs.dama(11).toArray());
		assertArrayEquals(Arrays.asList(82, 83, 84, 85, 86, 87, 88, 71, 61, 51, 41, 31, 21, 11, 72, 63, 54, 45, 36, 27, 18).toArray(), Escacs.dama(81).toArray());
		assertArrayEquals(Arrays.asList(78, 68, 58, 48, 38, 28, 18, 87, 86, 85, 84, 83, 82, 81, 77, 66, 55, 44, 33, 22, 11).toArray(), Escacs.dama(88).toArray());
		assertArrayEquals(Arrays.asList(28, 38, 48, 58, 68, 78, 88, 17, 16, 15, 14, 13, 12, 11, 27, 36, 45, 54, 63, 72, 81).toArray(), Escacs.dama(18).toArray());
		assertArrayEquals(Arrays.asList(24, 34, 44, 54, 64, 74, 84, 15, 16, 17, 18, 13, 12, 11, 25, 36, 47, 58, 23, 32, 41).toArray(), Escacs.dama(14).toArray());
		
		// peo
		assertArrayEquals(Arrays.asList(54).toArray(), Escacs.peo(44).toArray());
		assertArrayEquals(Arrays.asList(21).toArray(), Escacs.peo(11).toArray());
		assertArrayEquals(Arrays.asList().toArray(), Escacs.peo(81).toArray()); 
		assertArrayEquals(Arrays.asList().toArray(), Escacs.peo(88).toArray()); 
		assertArrayEquals(Arrays.asList(28).toArray(), Escacs.peo(18).toArray());
		assertArrayEquals(Arrays.asList(24).toArray(), Escacs.peo(14).toArray());
		
		// rei
		assertArrayEquals(Arrays.asList(54, 55, 45, 35, 34, 33, 43, 53).toArray(), Escacs.rei(44).toArray());
		assertArrayEquals(Arrays.asList(21, 22, 12).toArray(), Escacs.rei(11).toArray());
		assertArrayEquals(Arrays.asList(82, 72, 71).toArray(), Escacs.rei(81).toArray());
		assertArrayEquals(Arrays.asList(78, 77, 87).toArray(), Escacs.rei(88).toArray());
		assertArrayEquals(Arrays.asList(28, 17, 27).toArray(), Escacs.rei(18).toArray());
		assertArrayEquals(Arrays.asList(24, 25, 15, 13, 23).toArray(), Escacs.rei(14).toArray());		
	}
}
